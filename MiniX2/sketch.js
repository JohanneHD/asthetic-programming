function setup() {
createCanvas(800, 800);

//creating the background with all colors in the palette 
noStroke();
colorMode(RGB, 800, 800);
for (let i = 0; i < 800; i++) {
  for (let j = 0; j < 800; j++) {
    stroke(i, j, 0);
    point(i, j);
  }
}

}

function draw() {
//plotting in the head of the emoji
noStroke();
  fill(color(240, 184, 119));
    ellipse(400, 400, 700);
  fill(color(255, 255, 255));
    ellipse(250, 270, 180);
    ellipse(550, 270, 180);
//the mouth
    ellipse(400, 540, 500, 300);
//tooth
stroke(0, 0, 0);
  strokeWeight(2);
  fill(color(255, 255, 255));
    rect(340, 530, 50, 50);

noStroke();
  fill(color(240, 184, 119));
    ellipse(400, 440, 480, 220);
//nose
  fill(color(243, 150, 68));
    triangle(355, 400, 400, 260, 445, 400);
    ellipse(430, 385, 45);
    ellipse(370, 385, 45);
//toungue
stroke(204, 0, 0);
  strokeWeight(2);
  fill(color(255, 51, 51));
    ellipse(460, 650, 150, 50);
    ellipse(360, 650, 150, 50);


//left eye rolling with the mouse, within the eye.
noStroke();
let xc =constrain(mouseX, 210, 305);
let xs = constrain(mouseY, 220, 305);
fill(0);
circle(xc, xs, 50);

//Right eye rolling with the mouse, within the eye.
let xc1 =constrain(mouseX, 510, 605);
let xs1 = constrain(mouseY, 225, 305);
fill(0);
circle(xc1, xs1, 50);


}
