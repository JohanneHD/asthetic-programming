#MiniX4

Link to miniX: [minix](https://johannehd.gitlab.io/asthetic-programming/MiniX4/)

Link to Sketch: [link](https://gitlab.com/JohanneHD/asthetic-programming/-/blob/main/MiniX4/sketch.js)

Screenshot: ![screenshot](screenshot4.jpg)

Screenshot: ![screenshot](screenshot5.jpg)

For this weeks miniX I learned a thing or two about getting of the track, and ending up somewhere completely differet from where my thoughts startet out. I actually got so far of the track, that the minix no longer corrolates with this weeks topic, but either way I have made a program that I had fun making. The program isn't great, and it isn't good either, but i finnished it. I  had a lot of problems getting here, and struggled with syntaxes and functions. 

I made our world, and tried to express that the things we do, and how we live our lifes on earth, is making great impact in a bad sense. If we continue to evovle in this direction we will desroy the world, it's just a matter of time. This can be compaired with the war going on in the world right now, with ukraine and Rushia. this war between the two parts not only effects them, but it effects the whole world, both economically but also in a way how safe we feel. because accusations from all parts turn into threads and that turns into a conversation of atom veapons, and I don't know if the world can handle that. I made it so that if you move the mouse up, the rectangle to the right turns more red, and at the end the world explodes. Instead of using the example of atom veapons I lightent it up a bit, and used a whole other thing that is just as big of a problem. GLOBAL WARMING. we can't run from it, but one thing we can do, is change the way we live, and go by our day, 'cause if else, the thing in my programme will happen eventually. Maybe not that dramatic, but I think it is way better not to take that risk, and just start doing something about it. Therfore I have made this exploding world, which I call: "The end.... maybe". 

But to stick with the subject I have to slide away from my miniX, and talk about something completely different. Data. We live in a world where everything we do and say is captured and stored. People are either naive to this, or they are constantly looking over their shoulders. I know a lot of people my age covering their webcam with a sticker or another thing, to avoid someone or something being able to watch you and gather data from it. That's wrong on so many levels. How is it that we as citizens have to do that to feel a bit of privacy or safety? Why are we even being monitored? well as I see it, they watch everyone, with the exuse of our safety, but also to do simple thing as to know everything about each and everyone of us. This way we get commercials fit for us, recommendations for hobbies, art, and other aspects of our life, all in all, the computer knows more about us than we do ourselves.
