Link to miniX2 click [here](https://johannehd.gitlab.io/asthetic-programming/MiniX2/)

Link to sketch click [here](https://gitlab.com/JohanneHD/asthetic-programming/-/blob/main/MiniX%202/sketch.js)

screenshot: ![screenshot](emoji-miniX2.jpg)


#MiniX 2!

This is my second miniX and therfore my second readme, and I am starting to get the hang of some of the opponents of programming, but other things I find really difficult, and I can spend hours trying to figure out, where the problem is. 
the reason why is, even the smallest little error in the code, can result in there not showing anything up in the program you are trying to create. it takes time and effort to get to a level where you spot your errors at once or to not make any at all. and I am probably never going to get to that level of profession, but I hope to have great understanding in code moving forward, slowly but surely. 


In the program, I have made an emoji, with a colorful background. I call it googly eyes as the emoji has googly eyes, and they move with the mouse. I thought it was a fun feature, and I didn't want to focus on anything negative with all that's going on in the world right now... 
I've tried to focus on the goofy aspect of emojis, as I have always seen emojis as something to add to your text to make it livelier and more fun. Emojis are a whole new way of communicating, we even sometimes message each other back and forth using nothing but emojis, and still have a clear understanding of what one another is texting about. That's pretty cool, at least I think so. as emojis become more and more a part of our daily phone use, politics, equality, violation, and kultur, becomes a big topic. so instead of trying to shed a light on these issues, I have tried to do the opposite, by making a goofy one, an emoji, that pulls away from the serious, and is playful.     


In the making of this emoji, I found that I wanted a moving element, and that lead to a lot of research and failed tries to get the result that I wanted. I still feel like the moving googly eyes could have been better, and more presise, but I am pretty satisfied with what I have come up with. The Emoji is fun in my eyes, and it actually makes me feel like they should make emojis with little fun features, like moving eyes, hands or mouth, make them interact, instead of making new once. But when thinking about it, in a way they already have made interating emojis.
