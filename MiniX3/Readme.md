# My miniX3

Link to miniX3 click [here](https://johannehd.gitlab.io/asthetic-programming/MiniX3/)

Link to sketch click [here](https://gitlab.com/JohanneHD/asthetic-programming/-/blob/main/MiniX%203/sketch.js)

Screenshot: ![Screenshot](screenshot3.jpg)

When i first saw this weeks assignment, I instantly thought of the concept of time, and I got inspired by big clocks around the world. And my miniX for this week is highly inspirred by Big Ben in London. 
This was my first week exploring movement in more waysthan one, and i think it became okay. As earlier written I thought a lot about the concept of time, and how whatever context you are in, time passes, and I wanted to experiment with that, by making this clock, with different features to presend time. Time is never still, and continously spins around in the little circle that is the clock. Thefore instead of making a normal throbber as dots going around in a circle oror a line moving on the screen, but instead a kind of throbber, that actually shows you that you are waiting for something, and however long you are waiting, hence the concept of time. time is everyonse biggest enemy, when moving around on a day to day basis, such as brousing on the internet og moving around in traffic. Time is constant and we have to constantly be aware of the time, to know when you have a meeting, or when you are off from work or go to football. 

The throbber is a clock, and I made it move by defining time, and using the let function. let hr = hour(); let mn = minute(); and let sc = second(); these made it so the clock followed real time. then to make the arches and the lines I simplyy synced the lines and arches with the time that I just defined, and it was then pretty fun to play around with making a clock that llooked somewhat real, but also was welldesigned and thought out. it has no interaction, but it is a throbber, so that wasn't nessesery, as long as it shows you that time goes by.

I actually wanted to make something new and exciting, and invent a throbber on my own, but I gues this was one of those weeks where there were low floors. I made the best of it, and experientet with my level of expertise, and I brought in some help in the name of coding. I watched a youtube video by Daniel shiffman, where he explains the ups and downs of coding a clock, [watch it here](https://www.youtube.com/watch?v=E4RyStef-gY), and it really helped my process along, and gave me great understanding for the context 

What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?
A throbber is usually something you see, when you are waiting for a webside to load, downloading a file or any other thing is starting up such as your computers. And there are thousands if not millions of different throbbers created. To me it symbolizes 'wait while loading', as in no matter where or in what context you know if you see a throbber, you are about to wait at least a second befor sometthing starts up. it gives me some sort of sign that it truely is loading. I could imagine if there was just a blanc white screen, how many of us, would think that it wasn't loading and that somthing is  wrong. So I think the throbbers are geneious, such little spiraling objects, can say so much about time and space, and give the user some type of calmness. The knowledge that it will begin soon, but it isn't quite there yet.

One throbber that stands out to me is the throbber in snapchat, and the reason why, being that it is different from the "normal" kind of throbber, and I write that with "", because what is normal, we can make anything and use it as a throbber. In snapchat they use their icon, the yellow ghost, and it is looking at you, and then flying away with a tail og rainbow. it is fun, and and shows the waitng as well as a spiral of dots would have done so. So it is a great twist to make the app a litte fun and different.





