class Pineapple {
  constructor() {
    this.r = 150;
    this.x = width;
    this.y = height - this.r;
  }

  move() {
    this.x -= 10; //makes the pineapple move bacwards
  }

  show() {
    image(pineappleImg,this.x, this.y, this.r, this.r); //displays pineapples
  }
}
