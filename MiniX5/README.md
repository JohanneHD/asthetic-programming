#MiniX5

Link til MiniX5: [minix](https://johannehd.gitlab.io/asthetic-programming/MiniX5/)

Link til sketch: [link](https://gitlab.com/JohanneHD/asthetic-programming/-/blob/main/MiniX5/sketch.js)

![screenshot](screenshot6.jpg)
![screenshot](screenshot7.jpg)


The rules in my miniX for this week are: 
    (1) With the idea of only working with half the screen, draw 50% squares and 50% lines. 
    (2) Draw the squares and lines randomly after one another, and when reaching the end of the screen start over, under the first line
The program in itself is neverending, if you have a big enough computer the program could go on and on, but realisticaly the code stops running when it reaches the end of the screen. I would have like to make the program start over, when reaching that end, but my skillset wasn't there. how does it perform over time? well, the program will never be the same, every time you reset, the program makes another program, because of the use of randomness. I use [if random(1)<0.6{] and [else{], so that a majority of the objects apearing on the screen were to be squares. Other than that knowledge it is random wheather a square or a line appears.
I think that the rules I have come up with are very concrete, but I also see lots of other ways these rules can be interpreted. 

What role do rules and processes have in your work?
The rules have a big role, as the program is build upon them. If i hadn't layed out the rules before starting my programme, a whole other set of rules would have come out of it. That said, when using the rules, that I have set, I can imagine there can be a whole lot of different outcomes. I this weeks theme was fun, as you got to get creative when you have to sit down and really think about it, and come up with rule before starting your programme. I had to be very creative(which I love) but also be realistic, because if I just wrote down the first rules that came to mind, it would probably be impossible to make the programme, because I like to think very outside the box. So I thought it was fun to constrain the thought procces just a little bit.

Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter? after atending this weeks class Winnie spoke upon rules, and weather whoes the auther of the work/ programme that has been made. I thought that was very exciting to both read about and discuss. Because the question is, if you se a set of rules and deside to interpret them and make a programme/artwork out of the rules that has been made by someone else, is it really your work sinse it very likely won't look like others code, who also interpreted the rules? or is merely you using or imitating someone elses work? My opinion, well, I think that whoever or whatever we all get inspiration from somwhere when designing/programming/making things, and if this i imitating and copying code, then everything we make is copied. Nothing is original, and i refuse to think like that. In the end a little inspiration does not hurt anybody, and I think that the concept of rules and 100 of people interpreting one set of rules completly different is something quite beautiful. 


