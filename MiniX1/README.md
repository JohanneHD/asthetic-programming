# My first MiniX

Link to miniX click [here](https://johannehd.gitlab.io/asthetic-programming/MiniX1/)

Link to sketch click [here](https://gitlab.com/JohanneHD/asthetic-programming/-/blob/main/MiniX%201/sketch.js)

screenshot: ![Screenshot](2022-02-12__3_.png)

My First miniX represents an ice cream cone with a colorful background. To describe it further, the ice cream consists of three balls of ice cream and a cherry on top. The program is still, and you can therefore not interact with it and it can’t make any form of motion. In the beginning, it was the plan to make some sort of feature, but as I was working on the Minix I realized that my skill set wasn’t quite there yet. I focused on making the best stagnant program that I could instead, and really put my energy into that. I got inspiration from the p5.js reference page to make all the squares, triangles, and circles. 

The first thing I did when making this code, was establish the function Setup();, and I then created a canvas underneath there. This made it possible to even make a program, because now I had somewhere to work on, just like a canvas for an artist. You could say that a programmer is also an artist of their own. This function only runs once, and is great for defining the space you are going to work with. Afterward, I used the function Draw(); to enable the rest of the code to take place and make something happen on the canvas. This function is looping 60 frames per minute (the default setting), the function gets redrawn for each running frame. It was my first time programming anything, so even though it is nothing compared to what others may have made, I am proud of it, and I think I’ve tried a lot of new techniques and I have gotten to explore the so-called newfound language that programming is. 

My idea was to try and create an app icon and play with color and shape, as well as softness. When searching on the web to find the different colors I needed, I found that color as I used to know them, even as a person who paints in my spare time, had changed. There are thousands of combinations, and the starting point comes from just three basic colors red, green, and blue. Each color represents the numbers from 0-255, and from there you have endless combinations. For example, if you type in: fill(color(255, 128, 0)) you get the color orange. 

I am really amazed at how much work and lines of code goes into such a simple image of an ice cream cone, and it intrigues me to learn more and progress moving forward. With coding it seems everything is possible, it’s all about exploring and keeping an open mind. 
