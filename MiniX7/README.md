#MiniX7

Link to MiniX7 click [here](https://johannehd.gitlab.io/asthetic-programming/MiniX7/) 

link to sketch click [here](https://gitlab.com/JohanneHD/asthetic-programming/-/blob/main/MiniX7/sketch.js)


screenshot: ![Screenshot](Screenshot.png)

screenshot: ![Screenshot](Screenshot1.jpg)


Which MiniX have you reworked?

I have chosen to rework/look at the very first minix, just because I thought that would result in the biggest change, and I thought it was pretty borring to look at, så I took the opportunity to play around with it, and I am not saying it looks good, but it was fun to try all of the different syntaxes and do something different with the first minix.

What have you changed and why?

What I have essentially done is trying to make a bit of interaction with the piece, instead of it just being something to look at. So I started by deleting the colorful rainbow background, and played around with a for loop instead, because I think I am getting a hang of it, and wanted to try it for the background of the minix. som I made a lot of cirkles, popping up all over the background, and I placed the background(); in function setup(); so that the circles in draw, would always go on top of each other, and not be erased emediatly. I wanted the cirkles to be little scoops of icecream, but I hit a wall on that one, and couldn't figure out how to replace the circles with an image. But i then moved on to insert a cherry, and made it follow the mouse position, and made it into a little mini game/interaction, where one has to place the cherry on top of the icecream, and when that happens a gif shows up with fireworks, saying well done. This was made by doing the push(); and pop(); so that it didn't affect anything else in the code, and I used an if statement to tell the giff to show up, if the cherry was higher than a 100 pixels down on the canvas. 

What have you learnt in this mini X? How did you incorporate/advance/interprete the concepts in your ReadMe/RunMe (the relation to the assigned readings)?

I have learnt a few things in this minix, because I have used syntaxes I haven't used before, and also because I tried to play around and really get some of the things I didn't quite get before now. One thing  have learned is that push(); and pop(); are actually really useful, and I now understand that when using them you can control what gets effected by what, and isolate some of it to only effect itself. 

I have also really understood the principle of color, and how to really get creative or let the computer do the work and simply type in random, and then the number between 0-255, to get a random and different result, everytime you refresh the program. I thought that was a fun thing to play around with and see how the colors change as you change the numbers even just a little bit.

What is the relation between aesthetic programming and digital culture? How does your work demonstrate the perspective of aesthetic programming (look at the Preface of the Aesthetic Programming book)?

I think this class is great, because we get to learn programming skills, but also get an inside look at the programming world, and everyone in it. we have discussed a lot in class how this world have been dominated by white streight males, but we can see clearly that that is starting to change, communities are growing, and more people catch interest in the skill. the thing about programming and digital culture is how easy one can put something out into the world, and share work with the community, but also how everyone can take a piece of programming, and build upon that work, to better it or just play around like I just did with looking back at my very first minix. Programming can be so fun, both when haven bonderies and acting upon rules, but also when you draw outside the line, quite litterally. 
    "but is a means to engage with programming to question existing technological paradigms and further create changes in the technical system."
In this quote from winnie soons book 'Aesthetic Programming: an handbook of softwarestudies', I think they capture the essens very well. How everyone can engage with each others work, and use skill and knowledge to better different project, and create new things from old code, that maybe wasn't usefull, but can be.   
