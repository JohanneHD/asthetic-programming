function setup() {
  // put setup code here
  createCanvas(800, 800);
}

function draw() {
  //establishing the background
background(255, 0, 0);
  //to make sure there was no black lines on all edges.
noStroke();
  //Draw a rectangle at location (100, 0) with a side size of 100, and hight of 800.
  //making the background with lots of colors, by adding rectangles the hight of the background, and the with of 100
    fill(color(255, 128, 0));
  rect(100, 0, 100, 800);
    fill(color(255, 255, 0));
  rect(200, 0, 100, 800);
    fill(color(0, 255, 0));
  rect(300, 0, 100, 800);
    fill(color(0, 0, 255));
  rect(400, 0, 100, 800);
    fill(color(127, 0, 255));
  rect(500, 0, 100, 800);
    fill(color(255, 0, 255));
  rect(600, 0, 100, 800);
    fill(color(255, 0, 127));
  rect(700, 0, 100, 800);
    //drawing circles (as Ice cream balls) by adding the x and y, to where they go.
    stroke(252, 220, 134)
    fill(color(255, 255, 204));
  circle(285, 310, 250);
    stroke(188, 130, 97);
    fill(color(159, 116, 72));
  circle(515, 310, 250);
    stroke(255, 153, 153);
    fill(color(255, 204, 204));
  circle(400, 160, 250);
  //Making a tiangle (as the cone) by adding the x and the y to each corner of the triangle.
    noStroke();
    fill(color(209, 165, 108));
  triangle(150, 350, 650, 350, 400, 700);
  //making a cherry with a cirkle and a line.
  stroke(204, 0, 0);
  strokeWeight(6);
  fill(color(255, 0, 0));
  ellipse(400, 75, 110);
  stroke(76, 153, 0);
  fill(color(76, 153, 0));
  line(430, 75, 470, 85)
  //drawing lines in the waffle by adding the coordinates at the two points at each end of the lines.
  stroke(175, 112, 75);
  line(250, 350, 450, 633);
  line(350, 350, 500, 570);
  line(450, 350, 550, 500);
  line(550, 350, 600, 430);

  line(550, 350, 350, 633);
  line(450, 350, 300, 570);
  line(350, 350, 250, 500);
  line(250, 350, 200, 430);
}
