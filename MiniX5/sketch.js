let space = 30;
let x=0;
let y=0;
let R,G,B;

function setup() {
createCanvas(windowWidth,windowHeight);
background(random(255,0,255));
frameRate(30);
angleMode(DEGREES);
}

function draw(){
  rotate(-45);
stroke(random(255));
  if(random(1)<0.6){
    R = random(255);
    rect(x,y,space,space);
  } else {
    B = random(255);
    fill(random(255));
    line(x+space,y,x,y+space);
    line(x+space,y,x,y+space);
  }


  G = 0;
  fill(R,G,B);

  x+=space;
  if (x>width) {
    x = 0;
    y+=space;
  }
}
