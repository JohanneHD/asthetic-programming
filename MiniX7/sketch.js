let el = [];
let cherryImg;
let fireworksImg;

function setup() {
  // put setup code here
  createCanvas(800, 800);
  background(random(255),random(255),random(255));
  frameRate(3);
  cherryImg = loadImage("Cherry.png");
  fireworksImg = loadImage("fireworks.gif");
}

function draw() {
//making a for loop, to create a random and exciting background
    for(let i = 0; i < 10; i++){
    el[i] = new Circle(random(0,800),random(0,800),random(1,70));
    el[i].showCircle();
    }

    //drawing circles (as Ice cream balls) by adding the x and y, to where they go.
    stroke(252, 220, 134);
    strokeWeight(8);
    fill(color(255, 255, 204));
      ellipse(285, 310, 250);
    stroke(188, 130, 97);
    fill(color(159, 116, 72));
      ellipse(515, 310, 250);
    stroke(255, 153, 153);
    fill(color(255, 204, 204));
      ellipse(400, 160, 250);
  //Making a tiangle (as the cone) by adding the x and the y to each corner of the triangle.
    stroke(175,112,75);
    fill(color(209, 165, 108));
      triangle(150, 350, 650, 350, 400, 700);

  //drawing lines in the waffle by adding the coordinates at the two points at each end of the lines.
  stroke(175, 112, 75);
    line(250, 350, 450, 633);
    line(350, 350, 500, 570);
    line(450, 350, 550, 500);
    line(550, 350, 600, 430);

    line(550, 350, 350, 633);
    line(450, 350, 300, 570);
    line(350, 350, 250, 500);
    line(250, 350, 200, 430);
//the cherry - made to follow the position of the mouse
    push();
    image(cherryImg,mouseX,mouseY,80,120);
    pop();
    fill(random(255),random(255),random(255));
    stroke(random(255),random(255),random(255));
textSize(30);
    text("place the cherry on top of the ice cream cone",100,740);
//fireworks when cherry is placed correctly
    push();
    if (mouseY<100) {
      background(0);
    image(fireworksImg,0,0,800,800);
    text("WELL DONE", 300,400);
    }
    pop();
}

class Circle{
constructor(_xpos,_ypos,_size){
this.xpos = _xpos;
this.ypos = _ypos;
this.size = _size;
}

showCircle(){
push();
strokeWeight(random(1,20));
stroke(random(125,255),random(125,255),random(255));
fill(random(125,255), random(125,255), random(200,255));
rect(this.xpos,this.ypos,this.size,this.size,100);
pop();
}
}
