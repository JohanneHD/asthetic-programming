var angle = 0;

function setup() {
createCanvas(800, 800);
angleMode(DEGREES);

}
function draw() {

background(143, 211, 163);
translate(400,400);

noStroke();
//brick of big ben
rectMode(CENTER);
fill(color(255, 174, 87));
rect(0, 200, 400, 800);
fill(color(218, 156, 50));
rect(0, 0, 300, 300);
//roof of big ben
fill(color(95, 134, 95));
quad(-210, -200, 210, -200, 100, -400, -100, -400);
rect(0, -200, 420, 50);


//watch
fill(color(240));
ellipse(0, 0, 300, 300);


//Making the clock
  let hr = hour();
  let mn = minute();
  let sc = second();
rotate(-90);
noStroke();

//the arc of hours
fill(color(190));
let end1 = map(hr % 12, 0, 12, 0, 360);
arc(0, 0, 300, 300, 0, end1);

//the arc of minnutes
noStroke();
fill(color(210));
let end2 = map(mn, 0, 60, 0, 360);
arc(0, 0, 250, 250, 0, end2);

//the arc of seconds
noStroke();
fill(color(230));
let end3 = map(sc, 0, 60, 0, 360);
arc(0, 0, 200, 200, 0, end3);

//the line of seconds
push();
rotate(end3);
strokeWeight(5);
stroke(0);
line(0,0,50,0);
pop();

//the line of minnutes
push();
rotate(end2);
strokeWeight(5);
stroke(0);
line(0,0,70,0);
pop();

//the line of hours
push();
rotate(end1);
strokeWeight(5);
stroke(0);
line(0,0,90,0);
pop();

fill(color(218, 156, 50));
rect(0,-175,270,30,20);
rect(-270,-175,220,30,20);
rect(-270,-125,220,30,20);
rect(-240,-75,160,30,20);
rect(-240,-25,160,30,20);
rect(-240,25,160,30,20);
rect(-240,75,160,30,20);
rect(-270,125,220,30,20);
rect(-270,175,220,30,20);
rect(0,175,270,30,20);

rotate(90);
textSize(50);
text('BIG BEN',-100,370);



//rings around the clock
//noFill();
//strokeWeight(1);
//stroke(0);
//ellipse(0, 0, 300, 300);
//ellipse(0, 0, 250, 250);
//ellipse(0, 0, 200, 200);
}
