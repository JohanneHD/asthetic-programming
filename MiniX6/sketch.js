let pizza;
let pizzaImg; //pizza character drawing
let pineappleImg;
let backgroundImg; //bacground scenery drawing
let pineapple = []; //pineapple array
let score = 0;

function preload() {
  pizzaImg = loadImage('Pizza.png');
  pineappleImg = loadImage('Pineapple.gif');
  backgroundImg = loadImage('background.png');
}

function setup() {
  createCanvas(800, 600);
  pizza = new Pizza();
}

function keyPressed() {
  if (key == ' ') { //space key down
    pizza.jump(); //calling the jump function from pizza.js
  }
}

function displayScore(){
  fill(255);
  textSize(20);
  textStyle(NORMAL);
  textAlign(CENTER);
  text("SCORE = " + score, width-140, 50); //showing score in right corner
}

function draw() {
  if (random(1) < 0.008) { //0,8% of the time push new pineapples
    pineapple.push(new Pineapple());
  }

  for (let i = 0; i < pineapple.length; i++){

    let d = dist(pizza.x, pizza.y, pineapple[i].x, pineapple[i].y)
    print(d)
    if (d < 100){
      noLoop();
    }
    if (pineapple.x < -10){
      print("splice");
      pineapple[i].splice(0, 1)
    }
  }

  background(backgroundImg);
  fill(255);
  textSize(12);
  text("PRESS SPACE TO JUMP", width-140,80);
  noStroke();
  fill(255,80);
  rectMode(CENTER);
  rect(width-140,75,200,20);
  for (let e of pineapple) {
    e.move();
    e.show();
    if (pizza.hits(e)) { //if pizza hits a pinapple the game ends
      noLoop();
      textSize(50);
      textAlign(CENTER);
      textStyle(BOLD);
      fill(255);
      text("GAME OVER",width/2,height/2);
      textSize(20);
      text("BETTER LUCK NEXT TIME",width/2,height/2+40);
   }
    if (pizza.hits(e)==false){
      score++;
    }
  }


  pizza.show();
  pizza.move();
  displayScore();
}
