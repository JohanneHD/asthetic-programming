let space = 20;
let x=0;
let y=0;
let R,G,B;

function setup() {
createCanvas(windowWidth,windowHeight);
background(random(10,200));
frameRate(10);
}

function draw(){
stroke(random(255));
  if(random(1)<0.5){
    R = random(255);
    rect(x,y+space,x+space,y);
  } else {
    B = random(255);
    ellipse(x,y,x+space,y+space);
  }
  G = 0;
  fill(R,G,B,100);

  x+=space;
  if (x>width) {
    x = 0;
    y+=space;
  }
}
