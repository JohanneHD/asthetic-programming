#MiniX6

Link to MiniX6 click [here](https://johannehd.gitlab.io/asthetic-programming/MiniX6/) 

link to sketch click [here](https://gitlab.com/JohanneHD/asthetic-programming/-/blob/main/MiniX6/sketch.js)

link to the class Pizza click [here](https://gitlab.com/JohanneHD/asthetic-programming/-/blob/main/MiniX6/Pizza.js)

link to the class Pineapple click [here](https://gitlab.com/JohanneHD/asthetic-programming/-/blob/main/MiniX6/Pineapple.js)

screenshot: ![Screenshot](screenshot2.jpg)

In this weeks miniX I have worked on a game, and learned about classes and the term constructer. I've made a game about an object jumping over another object, and you have to dudge to keep playing the game. The game is very simple, and it is a pizzaslice dudging a pineapple, because he does not want it to go on the pizza. If the bad thing should happen that the pineapple goes on the pizzaslice, the game stops, and you loose. to manuvre in this game, all one has to do is press the space botton to jump up, and as soon as the game begins the pointcounter starts counting. the jump in action, was a very simple thing in comparison to some of the other components of the code. the only thing I did was use the function keyPressed(){

#Describe how you program the objects and their related attributes, and the methods in your game.

Okay so this MiniX was particularly difficult for me, and i found it hard to tell what was what, and the purpose of the objects and classes, but I finally made a game after 3 failed atempts. So, I startet my program by making a class, and named it Pizza (whith a capital letter, very important), then I went on to define/construct all about the pizza. the height, the width, the gravity when jumping and the speed og the jump. the show(){ syntax enabled me to insert a png image of the pizzaslice. I then moved on to make the class of the Pineapple(whith capital P) and did the same things here, only I made it move along the x-axis. This way it actually looks like the pizzaslize is moving forward, but actually it's x-axis always stays the same, and it is the pineapples that move. 

#Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?

At first when coming up with the idea of this program, I didn't put much thought into it, and didn't think that anyone else would either. it was just a piece of pizza that had to jump to dudge the pineapple, nothing more. or was it? When I sat down making this program one of my classmates bluured out "I  wouldn't ever eat a pizza with pineapples on it either" and we had a great laugh about how my game represended that very well. so I thought my gam was fun and relatable. well that was untill two classmates behind me stated that they loved pineapple on pizza, and thought it was the best. now all of a sutton my very suttle and childlike game was kind of controversial, and got people talking about it for quite some time. It got me thinking how small things can divide people, especially on the internet. small things like: "do you put your ketchup on top of the fries or at the side", and "do you drink pepsi or coca cola." I have even seen people form facebookgroups calling the group: "The people who sees the dress as blue and white". This got me thinking, do we do this as human? always divide each others into groups depending on what we like, what we se or even who we surround usself with. And at the end, maybe instead of acheaving the goal of making a group, the only thin it does is excluding everyone else. 

That quickly ran of the tracks, but maybe my childish game isn't as innocent as I thought it was, and is actually very politically provoking to some. Big or small, almost no matter what you put out there some will love it some will hate it, some will agree with you and some won't, that's both the beauty of the internet, but also sometimes the scary part.




