class Pizza {
  constructor() {
    this.d = 160 ; //diameter of circle
    this.x = 80;
    this.y = height - this.d;
    this.vy = 0 ; //velocity of speed along the y-axis
    this.gravity = 2; //adjusting the speed of pizza jump
  }

  jump() {
    //this only allows jump function to be called, when unicorn is touching the "ground"
    if (this.y == height - this.d) {
    //if jump function is activated, then pizza moves up y-axis
    this.vy = -40;
    }
  }

  hits(pizza) {
    //making collision circles for the pizza and the pineapple, with diameter d.

    return collideRectRect(this.x,this.y,this.d,this.d, pineapple.x, pineapple.y, pineapple.d, pineapple.d);
  }

  move() {
    this.y += this.vy;
    this.vy += this.gravity;
    //can only move up to a certain point
    this.y = constrain(this.y, 0, height - this.d);
  }

  show() {
    //displays the pizza avatar
    image(pizzaImg, this.x, this.y, this.d, this.d);
  }
}
